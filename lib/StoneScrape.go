package lib

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"log"
	"net/http"
	"strings"
	"strconv"
	"math"
)

func GetLocalsPage(page_id int) *http.Response {
	urlString := "https://www.stonegatepubs.com/find-your-local?page="

	resp, err := http.Get(fmt.Sprintf("%s%d", urlString, page_id))

	if err != nil {
		log.Fatal(err)
	}

	return resp
}

func GetDetailsPage(path string) *http.Response {
	urlString := "https://www.stonegatepubs.com"

	resp, err := http.Get(fmt.Sprintf("%s%s", urlString, path))

	if err != nil {
		log.Fatal(err)
	}

	return resp
}

func ProcessDetailsPage(resp *http.Response) (Pub, bool) {
	var pub = Pub{}

	doc, err := goquery.NewDocumentFromReader(resp.Body)

	if err != nil {
		log.Fatal(err)
	}

	doc.Find("div.banner-content div.row.column div.banner-caption div.banner-title h1 span.part-two").Each(func(index int, item *goquery.Selection) {
		pub.Name = item.Text()
	})

	var address []string

	doc.Find("html body div#venue div.row div.small-12.large-6.columns div.venue-intro div.page-summary ul.address.text-center li").Each(func(index int, item *goquery.Selection) {
		address = append(address, item.Text())
	})

	if len(address) <= 2 {
		return pub, false
	}

	pub.Postcode = address[len(address) - 1]
	pub.Address = strings.Join(address[0:len(address) - 2], ", ")

	doc.Find("html body div#venue div.row div.small-12.large-6.columns div.venue-intro div.page-summary div.opening-hours.text-center").Each(func(index int, item *goquery.Selection) {
		trimmedString := strings.TrimSpace(item.Text())
		times := strings.Split(trimmedString, "  ")
		for _, time := range times {
			day := strings.Split(time, ":")[0]
			parts := strings.Split(time, " ")
			openTime := "Closed"
			closeTime := "Closed"
			if len(parts) >= 3 {
				openTime = parts[1]
				closeTime = parts[3]
			}
			newOpenTimes := OpeningTime{
				Day:   day,
				Open:  openTime,
				Close: closeTime,
			}

			pub.OpeningTimes = append(pub.OpeningTimes, newOpenTimes)

		}

	})

	return pub, true

}

func GetPageCount(resp *http.Response) int {
	doc, err := goquery.NewDocumentFromReader(resp.Body)

	if err != nil {
		log.Fatal(err)
	}
	var pageCount int

	doc.Find("html body div.row.column div#find-local-search div.row.columns div.formats-intro div.search-result-number p").Each(func(index int, item *goquery.Selection) {
		parts := strings.Split(item.Text(), " ")
		pageCount, _ = strconv.Atoi(parts[len(parts) - 1])
	})

	pageCountRounded := float64(pageCount) / float64(10)

	return int(math.Ceil(pageCountRounded))

}

func ParseLocalsPage(resp *http.Response) []string {
	var pubUrls []string
	doc, err := goquery.NewDocumentFromReader(resp.Body)

	if err != nil {
		log.Fatal(err)
	}

	doc.Find("html body div.row.column div#find-local-search div.row.result div.small-6.medium-2.columns a.medium.button.expanded").Each(func(index int, item *goquery.Selection) {
		newUrl, exists := item.Attr("href")

		if exists == true {
			if string(newUrl[0]) == "/" {
				pubUrls = append(pubUrls, newUrl)
			}
		}
	})

	return pubUrls

}
