package lib

type Pub struct {
	Name string `json:"pub"`
	Address string `json:"address"`
	Postcode string `json:"postcode"`
	OpeningTimes []OpeningTime `json:"opening_times"`
}

type OpeningTime struct {
	Day string `json:"day"`
	Open string `json:"opens"`
	Close string `json:"closes"`
}
