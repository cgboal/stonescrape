package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/cgboal/stonescrape/lib"
	"sync"
	"golang.org/x/sync/semaphore"
	"context"
)

func main() {

	page := lib.GetLocalsPage(1)
	pageCount := lib.GetPageCount(page)
	var pubs []lib.Pub

	var wg sync.WaitGroup
	pubChannel := make(chan lib.Pub, (pageCount * 10))
	ctx := context.Background()
	lock := semaphore.NewWeighted(int64(10))

	for i := 1; i <= pageCount; i++ {
		wg.Add(1)
		lock.Acquire(ctx, 1)

		go func(i int, pubChannel chan lib.Pub) {
			defer wg.Done()
			defer lock.Release(1)
			page := lib.GetLocalsPage(i)
			localUrls := lib.ParseLocalsPage(page)

			for _, url := range localUrls {
				details := lib.GetDetailsPage(url)
				pub, success := lib.ProcessDetailsPage(details)
				if success == true {
					pubChannel <- pub
				}
			}

		}(i, pubChannel)
	}

	go func() {
		wg.Wait()
		close(pubChannel)
	}()

	for pub := range pubChannel {
		pubs = append(pubs, pub)
	}

	json, _ := json.Marshal(pubs)
	fmt.Printf(string(json))

}
